\documentclass{esannV2}
\usepackage{graphicx}
\usepackage[latin1]{inputenc}
\usepackage{amssymb,amsmath,array}
\usepackage{caption,subcaption}
\usepackage{array}
\usepackage[nodisplayskipstretch]{setspace}
\usepackage{amsmath}

\makeatletter
\g@addto@macro\normalsize{%
  \setlength\abovedisplayskip{5pt}
  \setlength\belowdisplayskip{5pt}
  \setlength\abovedisplayshortskip{5pt}
  \setlength\belowdisplayshortskip{5pt}
}
\makeatother
%***********************************************************************
% !!!! IMPORTANT NOTICE ON TEXT MARGINS !!!!!
%***********************************************************************
%
% Please avoid using DVI2PDF or PS2PDF converters: some undesired
% shifting/scaling may occur when using these programs
% It is strongly recommended to use the DVIPS converters, and to submit
% PS file. You may submit a PDF file if and only if you use ADOBE ACROBAT
% to convert your PS file to PDF.
%
% Check that you have set the paper size to A4 (and NOT to letter) in your
% dvi2ps converter, in Adobe Acrobat if you use it, and in any printer driver
% that you could use.  You also have to disable the 'scale to fit paper' option
% of your printer driver.
%
% In any case, please check carefully that the final size of the top and
% bottom margins is 5.2 cm and of the left and right margins is 4.4 cm.
% It is your responsibility to verify this important requirement.  If these margin requirements and not fulfilled at the end of your file generation process, please use the following commands to correct them.  Otherwise, please do not modify these commands.
%

\voffset 0 cm \hoffset 0 cm \addtolength{\textwidth}{0cm}
\addtolength{\textheight}{0cm}\addtolength{\leftmargin}{0cm}

%***********************************************************************
% !!!! USE OF THE esannV2 LaTeX STYLE FILE !!!!!
%***********************************************************************
%
% Some commands are inserted in the following .tex example file.  Therefore to
% set up your ESANN submission, please use this file and modify it to insert
% your text, rather than staring from a blank .tex file.  In this way, you will
% have the commands inserted in the right place.

\begin{document}
%style file for ESANN manuscripts
\title{Predictive Segmentation of Arabic Using Multichannel Neural Networks}

%***********************************************************************
% AUTHORS INFORMATION AREA
%***********************************************************************
\author{Mohamed A. Radwan, Mahmoud I. Khalil, and Hazem M. Abbas
%
% Optional short acknowledgment: remove next line if non-needed
%
% DO NOT MODIFY THE FOLLOWING '\vspace' ARGUMENT
\vspace{.3cm}\\
%
% Addresses and institutions (remove "1- " in case of a single institution)
Computers and Systems Engineering Department - Faculty of Engineering\\
Ain Shams University - Cairo, Egypt
}
%***********************************************************************
% END OF AUTHORS INFORMATION AREA
%***********************************************************************

\maketitle

\begin{abstract}
The problem of words segmentation in Arabic language, like many cursive languages, presents a challenge to OCR systems. Moreover, one Arabic character can have different shapes and characters are of different dimensions. This paper presents a multichannel neural network to solve offline segmentation of machine printed Arabic documents. The proposed network takes three consecutive windows of different widths as input to three channels. Then it predicts the likelihood of the middle window being a cut place. The model evaluation results on one font showed accuracy of 98.9\% while four fonts showed 95.5\% accuracy.
\end{abstract}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Start PAPER SQUEEZING - Use wisely
%% Source: http://www.terminally-incoherent.com/blog/2007/09/19/latex-squeezing-the-vertical-white-space/
% \setlength{\parskip}{0pt}
% \setlength{\parsep}{0pt}
% \setlength{\headsep}{0pt}
% \setlength{\topskip}{0pt}
% \setlength{\topmargin}{0pt}
% \setlength{\topsep}{0pt}
% \setlength{\partopsep}{0pt}

% % Remove space after figure and before text
 \setlength{\textfloatsep}{8pt}
 %\setlength{\floatsep}{6pt}
 %\setlength{\intextsep}{5pt}

%% Remove paragraphs auto-indentations
%\newlength\tindent
%\setlength{\tindent}{\parindent}
%\setlength{\parindent}{0pt}
%\renewcommand{\indent}{\hspace*{\tindent}}

%% DANGEROUS PARAMETER (linespread)
\linespread{0.95}
%% End PAPER SQUEEZING
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}
Automatic recognition of cursive text has always been a tough problem to solve \cite{alginahi2013survey}. Unlike Latin languages which are cursive mostly in handwritten text, Arabic and Farsi are cursive by nature. Typesetting is cursive in both machine generated and handwritten text. Segmentation of words to their constituting characters is a crucial step to the succeeding recognition phase. An Arabic character can have up to 4 different shapes according to its placement in the word: isolated, start, middle, and end (figure \ref{Fig:shapes}). Some characters may only differ in number of diacritics. Characters also have different height and width. Defined combinations of certain characters can have special ligatures to connect them (figure \ref{Fig:horlig}-\ref{Fig:verlig}). Due to these characteristics, segmentation algorithms can fall short by over-segmentation of wide characters, or under-segmentation of interleaving characters.
\begin{figure}
 \centering
 \begin{tabular}{ >{\centering\arraybackslash}m{1in}  >{\centering\arraybackslash}m{1in} >{\centering\arraybackslash}m{1in} }
    \begin{subfigure}[b]{0.15\textwidth}
        \includegraphics[width=\textwidth]{fourshapes.eps}
         \caption{}
        \label{Fig:shapes}
    \end{subfigure}&
    \begin{subfigure}[b]{0.15\textwidth}
        \includegraphics[width=\textwidth]{ligatures.eps}
        \caption{}
        \label{Fig:horlig}
    \end{subfigure}&
    \begin{subfigure}[b]{0.15\textwidth}
        \includegraphics[width=\textwidth]{ligatures-horz.eps}
        \caption{}
        \label{Fig:verlig}
    \end{subfigure}
  \end{tabular}
   \begin{tabular}{ >{\centering\arraybackslash}m{1in}  >{\centering\arraybackslash}m{1in} >{\centering\arraybackslash}m{1in}  >{\centering\arraybackslash}m{1in} }
 \begin{subfigure}[b]{0.13\textwidth}
        \includegraphics[width=\textwidth]{threshold.eps}
        \caption{}
        \label{Fig:threshold}
    \end{subfigure} &
    \begin{subfigure}[b]{0.13\textwidth}
        \includegraphics[width=\textwidth]{threshold2.eps}
        \caption{}
        \label{Fig:threshold2}
    \end{subfigure}&
    \begin{subfigure}[b]{0.15\textwidth}
        \includegraphics[width=\textwidth]{scale.eps}
        \caption{}
        \label{Fig:win_scale}
    \end{subfigure} &
 \begin{subfigure}[b]{0.15\textwidth}
        \includegraphics[width=\textwidth]{over.eps}
        \caption{}
        \label{Fig:win_over}
    \end{subfigure}
    \end{tabular}
     \caption{\subref{Fig:shapes}) Different shapes of two characters. \subref{Fig:horlig}-\subref{Fig:verlig}) Horizontal and vertical ligatures. \subref{Fig:threshold}) Threshold over-segmentation in left character. \subref{Fig:threshold2}) Threshold over-segmentation in right character, and under-segmentation in the middle. \subref{Fig:win_scale}) Character context and scale. \subref{Fig:win_over}) Small window over-segmentation effect.}
\end{figure}


Many algorithms devised for segmentation of cursive Arabic documents made use of the structural pattern of lower pixels density between characters. Based on this pattern, a histogram of horizontal projection has been widely used for segmentation \cite{amin1998off, zheng2004new}. However, this method is prone to over-segmentation when a character is composed of several ligatures, or under-segmentation due to the overlapping of characters (figure \ref{Fig:threshold}-\ref{Fig:threshold2}). Other structural segmentation algorithms depend on thinning or contour tracing \cite{margner1992sarat, azmi2001new} to extract strokes or angles and use them as features for extraction, this way they can solve the under-segmentation resulting from overlapping but suffers from over-segmentation. \cite{gouda2004segmentation, touj2007two} use HMM  to learn characters features and do implicit segmentation.

%Most of those algorithms were hand engineered algorithms and very few of them use learning systems. 
%Recently Neural Networks are shown to have state-of-the-art results in object recognition tasks \cite{lee2014deeply} by 
Neural networks learns hierarchical representations by stacking layers that learn features in each one from the output of previous layer. In this paper, multichannel neural network  \cite{ngiam2011multimodal} is used to learn from portions taken directly from the image by a sliding  window to predict their likelihood of being a candidate cut place for segmentation. Advances in training deep Neural Networks are exploited to reduce over-fitting. Convolutional layers \cite{lecun1998gradient} are used for learning features from the image. Regularization techniques like training data augmentation, and dropout \cite{srivastava2013improving} are used to enhance network generalization.

The purpose of this study is to build a model that can learn to explicitly segment Arabic words, of one font or preferably multiple fonts, into separated characters. The problem is formulated as a non-linear regression problem assigning a sliding window values between [-0.5,0.5] indicating confidence in segmentation area. 
%A 0.5 label means high confidence that the window is on segmentation area, while -5 means it's on a character. 
The network is blind to characters' classes as it only recognizes the features of segmentation candidate windows. The resulting model  for one font has 98.9\% segmentation accuracy and a model for four fonts has 95.5\% accuracy, and they are noticed to act well against over-segmentation and under-segmentation.
\section{Proposed Architecture}
The task is to recognize boundaries in-between characters of a word, given  its image. The typical OCR system can easily segment a document into lines then words. Using a scanning window on those words, the model predicts a likelihood of current window to be a segmentation area.
% Since Arabic characters differ in height and width, the shape of a character in a window is not fixed .
  The variances due to difference in characters' dimensions add lots of aberrations to how characters appear inside a window (figure \ref{Fig:win_scale}). Data augmentation techniques can be used to synthesize more data by applying some deformations on the train set \cite{simard2003best}. The functions chosen are scaling down or up, and translation on the Y-axis; so to simulate what might happen in test sets. 
%Because of the difference in width, a window that can contain one character can also contain more than one or only part of it. 
To solve over-segmentation a wider window is needed to recognize wide characters correctly (figure \ref{Fig:win_over}). Nonetheless, increasing the window too much will add information other than the location of the segmentation thus losing the cut localization, or under-segmentation.
Taking into consideration these problems two rules are established:
\begin{enumerate}
\setstretch{0.85}
  \item Have as much context inside a window without losing the cut localization.
  \item Maintain a design matrix that allows data augmentation without losing label consistency. 
\end{enumerate}
A neural network with single wide window as input will fail first point but allow second. A Recurrent neural network for sequence labelling of every horizontal point in the window, aside from being hard to train, it will fail second point. A model that uses three windows as input to a multichannel neural network is developed (figure \ref{Fig:model}). So beside using a small window for segmentation, another two channels are added as a previous window and a next window. This will increase the network input's context. Data augmentation is carried out by applying deformations to each window separately, without affecting the label consistency thus satisfying other point as well.
\begin{figure}
\centering
\includegraphics[scale=0.235]{net3.eps}
\caption{ Multichannel neural network for Arabic segmentation.}\label{Fig:model}
\end{figure}
\subsection{The Convolutional Channels}
Each channel learns low level features using convolutional layers \cite{lecun1998gradient}. Each layer contains a set of features mapping input to output by convolving a filter $w^{(l)}_{ij} $ of size $  A\times B $. $ Y^{(l)}_ijk$ is the component $j, k$ of feature map $i$ in $l$-th convolutional layer is given by:
\begin{equation}
 Y^{(l)}_{ijk} = max(0, B^{(l)}_i + \sum_{i'=1}^{m} \sum_{a=0}^{A} \sum_{b=0}^{B}{ w^{(l)}_{iab} * Y^{(l-1)}_{i'(j+a)(k+b)}}
\end{equation}
where $ max(0, x)$ is a rectified linear activation function \cite{glorot2011deep}, $ B_i^{(l)}$ is a bias term. The number of feature maps in previous layer is $m$,  $Y^{(l-1)}_{i'} $ is the output of the feature map $i'$ from the previous layer. A max pooling layer is added after the convolutional layers that sub-samples the max value of every 2x2 window. Then a dropout is used to reduce over-fitting of data that's not complex enough. It drops some neurons and their connections during training and found to increase the regularization of convolutional neural nets \cite{srivastava2013improving}.
\subsection{The Fully Connected Layer}
The model learns correlation between patterns that appear in each channel separately. The left channel should learn left parts of the characters, the right channel learns right parts of characters, and the middle learns the area in-between. A dense fully connected layer then learns association between each channel's specific features to find correlation between the three of them \cite{ngiam2011multimodal}
\begin{equation}
 Y^{(h)}_i = max(0, W_M Y_M + W_L Y_L + W_R Y_R + B^{(h)}_i )
\end{equation}
where ${Y_x , W_x}$ are the output of a channel and the weight associated with it for each x $ \in $ \{L,M,R\} the left, middle, and right layers respectively. Left and right channels can be thought of as a bias that this layer learns for middle channel features. At the end there is a regression layer that learns the likelihood of the window to be a segmentation place. 

Two models are constructed from training on two different training sets described in the next section. Stochastic gradient descent is used to minimize mean squared error loss function. During back propagation, each channel has its own separate weights update in learning phase, allowing each to learn separate features.
\section{Model Evaluation}
To test this segmentation method two models are constructed: The first model is trained on one font, and the second model on four fonts. Then, two test sets are used on both models, the first contains words written in one font and the other in four fonts.
\subsection{Data Generation}
Using fonts to draw words programmatically, the ground truth of segmentation line can be extracted by drawing characters iteratively and finding the width each time (figure \ref{Fig:segment}). For each segmentation ground truth, a mean of a normal function is assigned at it (figure \ref{Fig:seg_plot}). For each word the data is generated by having a sliding window go over it. Each window taken as a middle window plus the a window to its left and right are added as an example. Each example will have label -0.5 (figure \ref{Fig:classzero}) if its inside a character boundary. Otherwise if the middle window starts in a segmentation place, then its label is given by the normal function assigned to this place(figure \ref{Fig:classone}). A function used to assign a label for each window place on a word is shown in figure \ref{Fig:seg_plot}. This algorithm is repeated for 2100 words, each word having $Wn$ random characters where $Wn \in \{2,3,4,5\}$.Third of these words are held out as a validation data set.
\begin{figure}
\centering
\begin{tabular}{c c c c c}
    \begin{subfigure}{0.12\textwidth}
        \includegraphics[width=\textwidth]{segment.eps}
         \caption{}
        \label{Fig:segment}
    \end{subfigure}&
    
    \begin{subfigure}{0.1\textwidth}
        \includegraphics[width=\textwidth]{win/figure_1.eps}
         \caption{}
        \label{Fig:word1}
    \end{subfigure}&
    \begin{subfigure}{0.25\textwidth}
        \includegraphics[width=\textwidth]{seg_plot.eps}
         \caption{}
        \label{Fig:seg_plot}
    \end{subfigure}&
    \begin{subfigure}{0.08\textwidth}
        \includegraphics[width=\textwidth]{win/figure_4.eps}
        \includegraphics[width=\textwidth]{win/figure_5.eps}
        \includegraphics[width=\textwidth]{win/figure_6.eps}
        \includegraphics[width=\textwidth]{win/figure_7.eps}
        \caption{}
        \label{Fig:classzero}
    \end{subfigure}&
    \begin{subfigure}{0.1\textwidth}
        \includegraphics[width=\textwidth]{win/figure_2.eps}
        \includegraphics[width=\textwidth]{win/figure_3.eps}
        \caption{}
        \label{Fig:classone}
    \end{subfigure}
  \end{tabular}
\caption{\subref{Fig:segment}) Finding ground truth of segmentation. Data are extracted from (\subref{Fig:word1}) by sliding window. Function in (\subref{Fig:seg_plot}) is used for assigning labels. Examples in (\subref{Fig:classzero}) are labelled as -0.5, (\subref{Fig:classone}) are of labels greater than 0.}
\end{figure}

The data selected for the training set is used for augmentation procedure. Four deformations are used to synthesize new data: scaling up, scaling down, moving in positive direction of Y, and moving in negative direction of Y. For each training example, three of these deformations are selected randomly to apply one on each window and add the result as a new example with the same label. The size of data with labels greater than zero are increased explicitly to balance them with the data of labels less than zero. Augmentation increases the training data and leads to better generalization on test set \cite{simard2003best}.

The first model uses Arial font in the generation. The second model uses the following four fonts: Arial, Tahoma, Thuluth, and Damas, which are very different in characteristics. The test sets are generated similarly but using about 250,000 real Arabic words from a dictionary. These words range in length from two to six characters. Two test sets are generated, first using the Arial font, and the other test set is generated using same previous four fonts.
\subsection{Experimental results}
The loss function values of each trained model are listed in table \ref{Tab:results} for training, validation, and two test sets. Besides reporting the loss of the test sets, an accuracy is also reported to give some sense of the model's performance. The test set is converted into classification by checking if the network would assign the segmentation ground truth (figure \ref{Fig:segment}) with labels greater than $0.2$. This accuracy is reported on both test sets in the table. The 1-Font model is able to segment 4-fonts test set with accuracy of 65.5\%, so it's able to get around 40\% of the other fonts' segments right.

\begin{table}[]
\centering
\resizebox{\textwidth}{!}{%
\begin{tabular}{l|c|c|l|c|l|c|c|l|c|}
\cline{2-10}
                                     & Recall & \multicolumn{2}{c|}{Validation} & \multicolumn{3}{c|}{1-Font Test}                   & \multicolumn{3}{c|}{4-Fonts Test}                   \\ \cline{2-10} 
                                     & Loss   & \multicolumn{2}{c|}{Loss}       & \multicolumn{2}{c|}{Loss}   & Acc L\textgreater0   & \multicolumn{2}{c|}{Loss}   & Acc L\textgreater0    \\ \hline
\multicolumn{1}{|c|}{1-Font Model}   & 0.0247 & \multicolumn{2}{c|}{0.0315}     & \multicolumn{2}{c|}{0.0335} & 98.9\%               & \multicolumn{2}{c|}{0.110}  & 65.8\%                \\ \hline
\multicolumn{1}{|c|}{4-Fonts Model} & 0.0301 & \multicolumn{2}{c|}{0.0309}     & \multicolumn{2}{c|}{0.0431} & 98.7\%               & \multicolumn{2}{c|}{0.0465} & 95.5\%                \\ \hline
\end{tabular}
}
\caption{Resulting of loss values and (Acc $L>0$) accuracy of a test data with labels greater than $0$.}
\label{Tab:results}
\end{table}
Figure \ref{Fig:result} shows correct segmentation of a sample document containing previously over-segmented and under-segmented characters. The data examples that maximally activates neurons in the left channel are shown in (figure \ref{Fig:grid1}), and the right channel in (figure \ref{Fig:grid2}). The windows that maximally activate the left channel are mostly left parts of characters that appear next to a cut. The right channel learns right parts of the character that appear right to a cut place.

\begin{figure}
 \centering
    \begin{subfigure}[b]{0.6\textwidth}
        \includegraphics[width=\textwidth]{result2.eps}
        \caption{}
        \label{Fig:result}
    \end{subfigure}\\
 \begin{tabular}{ >{\centering\arraybackslash}m{2in}  >{\centering\arraybackslash}m{2in} }
    \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{maximal_left2.eps}
         \caption{}
        \label{Fig:grid1}
    \end{subfigure}&
    \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{maximal_right2.eps}
        \caption{}
        \label{Fig:grid2}
    \end{subfigure}
  \end{tabular}
\caption {\subref{Fig:result}) Segmentation of a document using the model. \subref{Fig:grid1}) Segments of words that have maximal neural respone in left channel, and right channel in (\subref{Fig:grid2}). }\label{Fig:maximal}
\end{figure}


\section{Conclusion}
In segmentation of cursive text, a multichannel neural network that incorporates the previous and next contexts of the window it's trying to predict will overcome over-segmentation and under-segmentation problems. A network trained to segment one Arabic font has an accuracy of 99\%. The more you increase the fonts the lower this accuracy can decrease specially if they are typographically dissimilar. A network for segmenting four fonts has 95.5\% accuracy.
%This model can be very useful for building a complete OCR system for specific fonts, as the generation and training process can be autonomous, there are no many variables or features to be engineered. 
\begin{footnotesize}
\setstretch{0.85}
\parskip 0pt 
%\parskip 0pt 
%\linespread{0.9}
\setlength{\textfloatsep}{0pt}
%\setlength{\floatsep}{0pt}
\setlength{\intextsep}{3pt}
% IF YOU USE BIBTEX,
% - DELETE THE TEXT BETWEEN THE TWO ABOVE DASHED LINES
% - UNCOMMENT THE NEXT TWO LINES AND REPLACE 'Name_Of_Your_BibFile'

\bibliographystyle{unsrt}
\bibliography{arabic_deep}

\end{footnotesize}

% ****************************************************************************
% END OF BIBLIOGRAPHY AREA
% ****************************************************************************

\end{document}
